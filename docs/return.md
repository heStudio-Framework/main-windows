# 返回码

在这里，你可以查找框架的返回值。

| 返回码 | 返回原因 |
|---|---|
| 20000 | 正常运行结束 |
| 21099 | 部分窗口组件已被用户主动关闭 |
| 25000 - 25999 | 错误码，请到 [error.md](https://gitee.com/hestudio-framework/main-windows/blob/master/docs/error.md) 查看 |

### 面向框架开发者的内容
如果你在开发的时候发现没有你要使用到的返回代码，请以2xxxx为返回码，并不得重复。