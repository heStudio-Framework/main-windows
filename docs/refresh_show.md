# refresh_show

### 简介
一个可以随时变化的文本框

### 参数
- `title`(可选)标题
- `geometry`(可选)分辨率，格式同`tkinter`的`geometry`
- `message`每次传入的消息

### demo
```python
# 导入模块
import heframework

# 创建窗口（实体化对象）
a = heframework.refresh_show(title="title", geometry="200x100")

# 更新文字
a.refresh("hello world")
```

![1672123898135.png](https://image.hestudio.org/img/2022/12/27/63aa95ff44478.png)

### 参加本模块的名单（请自行添加）：
[醉、倾城](https://www.hestudio.org/about)