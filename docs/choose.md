# choose

### 功能
列举设定的程序组，并按照用户的输入值返回对应的值

### 使用方法
#### mode
| str | infomation |
|---|---|
| list (defeat) | 通过传入`list`为框架提供数据 |
| json | 通过json文件代替两个`list` |

#### list方式
```python
# 设定数据
name =  ["name1", "name2", "name3"],
return_text =  ["return_text1", "return_text2", "return_text3"]

# 调用框架
import heframework
heframework.choose(mode="list", name=name, return_text=return_text, info="demo")
```
#### json方式
demo.json
```json
{
    "name": ["name1", "name2", "name3"],
    "return_text": ["return_text1", "return_text2", "return_text3"]
}
```
demo.py
```python
import heframework
heframework.choose(mode="json", json_file="demo.json", info="demo")
```
其中，json_file是按照上述格式的json文件，而info是在选择程序时显示的文本。

### Demo
```python
# 导入框架
import heframework

# 引入数据
name =  ["name1", "name2", "name3"],
return_text =  ["return_text1", "return_text2", "return_text3"]

# 编写demo
demo_show = heframework.choose(mode="list", name=name, return_text=return_text, info="Choose you choice >>>")
print(str("输出的文本是", demo_show))
```

![1671959603435.png](https://image.hestudio.org/img/2022/12/25/63a81436981d4.png)

### 最近一次更新
| 版本号 | 更新内容 |
|---|---|
| 0.5.1 | 修复choose在json模式下返回None的错误 |


### 参加本模块的名单（请自行添加）：
[醉、倾城](https://www.hestudio.org/about)