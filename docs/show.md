# show

### 功能
弹出通知对话框

### 要求
仅支持有视窗界面的系统

### 使用方法

```python
import heframework
heframework.show(message="demo", title="demo")
```

其中，message是在消息框输出的消息，而title是消息框的标题。如果我们运行上面的代码，就会得到以下结果。

![1670978131541.png](https://image.hestudio.org/img/2022/12/14/63991a5d5c1d6.png)

### 最近一次更新
| 版本号 | 更新内容 |
|---|---|
| 0.2.11 | 修复在不传入标题无法运行的问题 |

### 参加本模块的名单（请自行添加）：
- [醉、倾城](https://www.hestudio.org/about)
- [emmm](https://www.hestudio.org/talking)