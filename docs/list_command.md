# list_command

### 功能
列举设定的程序组，并按照用户的输入值执行对应的程序。
![1671070801818.png](https://image.hestudio.org/img/2022/12/15/639a84564f2d2.png)

### 使用方法
#### mode
| str | infomation |
|---|---|
| list (defeat) | 通过传入`list`为框架提供数据 |
| json | 通过json文件代替两个`list` |

#### list方式
```python
# 设定数据
name =  ["name1", "name2", "name3"],
command =  ["command1", "command2", "command3"]

# 调用框架
import heframework
heframework.list_command(mode="list", name=name, command=command, info="demo")
```
#### json方式
demo.json
```json
{
    "name": ["name1", "name2", "name3"],
    "command": ["command1", "command2", "command3"]
}
```
demo.py
```python
import heframework
heframework.list_command(mode="json", json_file="demo.json", info="demo")
```
其中，json_file是按照上述格式的json文件，而info是在选择程序时显示的文本。


### 最近一次更新
| 版本号 | 更新内容 |
|---|---|
| 0.3.0 | 1. 修改名称为`list_command`; 2. 新增list格式支持 |
| 0.2.10 | 修复对中文的支持 |

### 参加本模块的名单（请自行添加）：
[醉、倾城](https://www.hestudio.org/about)