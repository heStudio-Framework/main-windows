# heStudio Framework

### 介绍
heStudio自研公共框架。

### 更新内容
[![heStudio FrameWork/main-windows](https://gitee.com/hestudio-framework/main-windows/widgets/widget_card.svg?colors=050200,ffffff,ffffff,e3e9ed,666666,9b9b9b)](https://gitee.com/hestudio-framework/main-windows)

### 使用前注意
请在使用框架的时候先查看它支持什么系统，关于图形界面的均不完全兼容Linux，所有框架均无法保证能在Mac OS上使用（没有Mac电脑）。

### 如何安装
```
pip install heframework
``` 

### 如何调用
点击下面框架名称查看各个框架的使用方法

### 框架内容
| 名称 | 功能 | 开坑时间 | 最近一次填坑时间 | 最后更新版本 | 最低支持版本 | 坑主 |
|---|---|---|---|---|---|---|
| [show](https://gitee.com/hestudio-framework/main-windows/blob/master/docs/show.md) | 弹出通知对话框 | 2022.12.14 | 2022.12.18 | 0.4.0 | 0.2.6 |[醉、倾城](https://www.hestudio.org/about) |
| [list_command](https://gitee.com/hestudio-framework/main-windows/blob/master/docs/list_command.md) | 列举设定的程序组，并按照用户的输入值执行对应的程序 | 2022.12.14 | 2022.12.18 | 0.3.0 | 0.2.6 |[醉、倾城](https://www.hestudio.org/about) |
| [choose](https://gitee.com/hestudio-framework/main-windows/blob/master/docs/choose.md) |  列举设定的程序组，并按照用户的输入值返回对应的值 | 2022.12.24 | 2022.12.27 | 0.5.1 | 0.4.0 | [醉、倾城](https://www.hestudio.org/about) |
| [refresh_show](https://gitee.com/hestudio-framework/main-windows/blob/master/docs/refresh_show.md) | 一个可以随时变化的文本框 | 2022.12.27 | 2022.12.27 | 0.5.0 | 0.5.0 | [醉、倾城](https://www.hestudio.org/about) |

### 报错查询
返回码查询：https://gitee.com/hestudio-framework/main-windows/blob/master/docs/return.md

如果你的程序在运行框架时返回了以25开头的报错代码时，你可能在[error.md](https://gitee.com/hestudio-framework/main-windows/blob/master/docs/error.md)找到解决方案，如果是其他的反馈，请通过[heStudio Talking](https://www.hestudio.org/talking)或者向本仓库提交issue反馈。

### 参与人员
- [醉、倾城](https://www.hestudio.org/about)
- [emmm](https://www.hestudio.org/talking)

### 如何参与
向本仓库提交PR即可。

### 提交建议
请通过 https://gitee.com/hestudio-framework/feature/ 的issue提交对于下一个版本的功能要求。Bug问题请通过[heStudio Talking](https://www.hestudio.org/talking/)反馈。

### 赞助
[![](https://image.hestudio.org/img/2022/12/13/639873ce2d116.jpg)](https://afdian.net/a/hestudio)