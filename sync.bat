@echo off
rd /s /q heframework
rd /s /q dist
rd /s /q heframework.egg-info
md heframework
md heframework\src
copy heframework.py heframework\
copy src\*.py heframework\src\
cd heframework
ren heframework.py __init__.py
cd ..